/*  ***** BEGIN LICENSE BLOCK *****
**	Copyright (C) 2012 2015 s3v
**
**    This file is part of "DebSearchToolbar".
**    You may find the license in the LICENSE file
**
    ***** END LICENSE BLOCK *****  */

// Event listeners
window.addEventListener("load", function () {
	gBrowser.addEventListener("DOMContentLoaded", caricamento, false);
	gBrowser.tabContainer.addEventListener("TabSelect", caricamento, false);
	gBrowser.addEventListener("pageshow", caricamento, false);
}, false);

// Import module for autocompleting feature
Components.utils.import('resource://gre/modules/FormHistory.jsm');



//**************************************************************************************
// Return URL of the current page
//**************************************************************************************
function currenturl() {
	return getBrowser().selectedBrowser.contentWindow.location.href.toString();
}



//**************************************************************************************
// Return current source page
//**************************************************************************************
function sourcePage() {
    return window.content.document.body.innerHTML.toString();
}



//**************************************************************************************
// Return 1 whether current page is a bug report log. 0 otherwise.
//**************************************************************************************

function isabug() {
	var myurl = currenturl();
	if (myurl.indexOf("https://bugs.debian.org/cgi-bin/bugreport.cgi?") >= 0)
		return 1
	else
		return 0
}



//**************************************************************************************
// Go to URL (specified in current tab)
//**************************************************************************************
function gotoURL(address) {
	getBrowser().webNavigation.loadURI(address, null, null, null, null);
}



//**************************************************************************************
// Enable button.
// First parameter -> button id
//**************************************************************************************

function enableBtn(bottone) {
	switch (bottone) {
		case "myDST_PrevBtn":
			var immagine = "up.png";
			break;
		case "myDST_NextBtn":
			var immagine = "down.png";
			break;
		case "myDST_ptspack":
			var immagine = "right.png";
			break;
	}

	document.getElementById(bottone).removeAttribute("disabled");
	document.getElementById(bottone).image = "chrome://debsearchtoolbar/skin/img/" + immagine;
}



//**************************************************************************************
// Disable button.
// First parameter -> button id
//**************************************************************************************

function disableBtn(bottone) {
	switch (bottone) {
		case "myDST_PrevBtn":
			var immagine = "up2.png";
			break;
		case "myDST_NextBtn":
			var immagine = "down2.png";
			break;
		case "myDST_ptspack":
			var immagine = "right2.png";
			break;
	}

	document.getElementById(bottone).setAttribute("disabled", "true");
	document.getElementById(bottone).image = "chrome://debsearchtoolbar/skin/img/" + immagine;
}



//**************************************************************************************
function caricamento() {

	if (isabug()) {
		var myurl = currenturl();

		// Delete number sign (if any) followed by numbers at the end of URL
		// i.e. https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=323223#20 becomes
		// https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=323223
		var myurlbug = myurl.replace(/#\d+/g, "");
		
		// anchor of bug report message (if any). i.e. 5, 10, 15, 20 ...
		var myurlnum = myurl.replace(/https:.*bug=[\d]*#?/, "");

		// bug report number (i.e. 323223)
		var splstr = myurlbug.split(/https:\/\/bugs\.debian\.org\/cgi-bin\/bugreport\.cgi\?.*bug=/)[1];
		
		if (myurlnum == "") {
			// no anchor. i.e. https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=323223
			index = -1;
			disableBtn("myDST_PrevBtn");    // Disabilita myDST_PrevBtn
			enableBtn("myDST_NextBtn");     // Abilita myDST_NextBtn
		}
		else {
			// bug report page with anchors. i.e. https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=323223#20
			var numArr = anchorArray();
			var dim = numArr.length;
			
			// anchor position
			var pos = numArr.indexOf(myurlnum);
			
			index = pos;
			
			// first item
			if (pos == 0) {
				disableBtn("myDST_PrevBtn");
				enableBtn("myDST_NextBtn");
			}
			// last item
			else if (pos == dim -1) {
				enableBtn("myDST_PrevBtn");
				disableBtn("myDST_NextBtn");
			}
			else {
				enableBtn("myDST_PrevBtn");
				enableBtn("myDST_NextBtn");
			}
		}

		document.getElementById("myDST_textboxbug").value = splstr;

		
		var pkgName = returnpack();
		if (isPseudoPkg(pkgName))
			disableBtn("myDST_ptspack");			
		else
			enableBtn("myDST_ptspack");				
	}
	else {
		disableBtn("myDST_PrevBtn");
		disableBtn("myDST_NextBtn");
		disableBtn("myDST_ptspack");

		document.getElementById("myDST_textboxbug").value = "";
	}
}



//**************************************************************************************
function Cerca(event) {
	var ricerca = document.getElementById("myDST_Tipo_ricerca").value;
	var query = document.getElementById("myDST_textbox").value;
	var nome = document.getElementById("myDST_menu_nome").value;
	var suite = document.getElementById("myDST_menu_suite").value;
	var sezione = document.getElementById("myDST_menu_sezione").value;

	document.getElementById("myDST_textbox").value = "";

	var address = "";

	if (ricerca == "ricerca_nomefile")
		address = "http://packages.debian.org/search?suite=stable&arch=any&mode=filename&searchon=contents&keywords=" + query
	else if (ricerca == "ricerca_pts")
		address = "https://tracker.debian.org/" + query
	else if (ricerca == "ricerca_pack")
		address = "http://packages.debian.org/search?suite=" + suite + "&section=" + sezione + "&searchon=" + nome + "&keywords=" + query
	else
		address = "http://sources.debian.net/src/" + query
		
	gotoURL(address)
}



//**************************************************************************************
// Add search term to the autocompleting items list
//**************************************************************************************
function aggiungi() {
	FormHistory.update({op: "bump", fieldname: "myautocomplete", value: document.getElementById("myDST_textbox").value});
}



//**************************************************************************************
// Code executed when enter key is pressed into package search textbox
//**************************************************************************************
function autocom(event) {
	if (event.which == 13 && document.getElementById("myDST_textbox").value != "") {
		aggiungi();
		Cerca(event);
	}
}



//**************************************************************************************
// Code executed at search button click
//**************************************************************************************
function clic(event) {
	if (document.getElementById("myDST_textbox").value != "") {
		aggiungi();
		Cerca(event);
	}
}



//**************************************************************************************
// Open bug report page when enter key is pressed into bug textbox
//**************************************************************************************
function onkeypressbug(event) {
	if (event.which == 13)
		clicbug(event)
}



//**************************************************************************************
// Open the bug report page
//**************************************************************************************
function clicbug(event) {

	var testo = document.getElementById("myDST_textboxbug").value;

	// string in textbox must be a number and not empty
	var test = isNaN(testo);

	if ((!test) && (testo != ""))
		gotoURL("https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=" + testo)
}



//**************************************************************************************
// Return an array related to the anchors that are in current source page
// i.e. [5, 10, 15, 20, 25]
//**************************************************************************************
function anchorArray() {
	var regex = /(<hr>.*Message #)(\d*)(.*)/gi;
	var input = sourcePage();

	var matches = input.match(regex);
	var dim = matches.length;
	for (var i = 0; i < dim; i++) {
		myvar = matches[i];
		myvar = myvar.replace(regex, "$2");
		matches[i] = myvar;
	}
	
	return matches
}



//**************************************************************************************
// Return package name located in current source page 
//**************************************************************************************

function returnpack() {
	var stringa = sourcePage().match(/submitter.*</g)[0];
	var pacchetto = stringa.substring(stringa.lastIndexOf(">") + 1, stringa.lastIndexOf("<"));
	
	// if package name begins with "src:", then first four characters will be deleted
	// (i.e. src:packagename -> packagename)
	if (pacchetto.substring(3, 4) == ":") {
		var pacchetto2 = pacchetto.substring(4);
		return pacchetto2;
	}
	else return pacchetto.toString();
}



//**************************************************************************************
// Pseudo-packages array. This type of packages disables PTS button.
// See: https://www.debian.org/Bugs/pseudo-packages
//**************************************************************************************
function isPseudoPkg(nomepkg) {
	
	// Pseudo-packages array
	var pseudoPkgArr = ["base",
				"bugs.debian.org",
				"buildd.debian.org",
				"buildd.emdebian.org",
				"cdimage.debian.org",
				"cdrom",
				"cloud.debian.org",
				"debian-i18n",
				"debian-live",
				"debian-maintainers",
				"d-i.debian.org",
				"ftp.debian.org",
				"general",
				"installation-reports",
				"kernel",
				"lists.debian.org",
				"mirrors",
				"nm.debian.org",
				"pet.debian.net",
				"piuparts.debian.org",
				"press",
				"project",
				"qa.debian.org",
				"release.debian.org",
				"release-notes",
				"security.debian.org",
				"security-tracker",
				"snapshot.debian.org",
				"sponsorship-requests",
				"sso.debian.org",
				"summit.debconf.org",
				"tech-ctte",
				"tracker.debian.org",
				"upgrade-reports",
				"wiki.debian.org",
				"wnpp",
				"www.debian.org"];

	var res = pseudoPkgArr.indexOf(nomepkg);
	if (res == -1)
		return 0 // isn't a pseudo-package
	else
		return 1 // pseudo-package!
}



//**************************************************************************************
// Open PTS page at button click
//**************************************************************************************
function clicptspack() {
	var pacchetto = returnpack();
	gotoURL("https://tracker.debian.org/pkg/" + pacchetto);
}



//**************************************************************************************
// Code executed when "previous" button is pressed
//**************************************************************************************

function clicbugprev() {

	// This block is executed only if PrevBtn is enabled
	if (document.getElementById("myDST_PrevBtn").disabled == false) {

		var matches = anchorArray()
		var dim = matches.length;

		index--;

		if (index >= 0) {

			if (index < dim - 1)
				enableBtn("myDST_NextBtn");
			else
				disableBtn("myDST_NextBtn");

			prev = parseInt(matches[index], 10);
			var miourl = currenturl();

			// Delete number sign (if any) followed by numbers at the end of URL
			miourl = miourl.replace(/#\d+$/g, "");

			var variabile = miourl + "#" + prev;

			gotoURL(variabile);

			if (index == 0)
				disableBtn("myDST_PrevBtn");
		}
	}

}



//**************************************************************************************
//Code executed when "next" button is pressed
//**************************************************************************************
function clicbugnext() {

	// This block is executed only if NextBtn is enabled
	if (document.getElementById("myDST_NextBtn").disabled == false) {

		var matches = anchorArray()
		var dim = matches.length;
		
		index++;

		if (index < dim) {

			if (index > 0)
				enableBtn("myDST_PrevBtn");
			else
				disableBtn("myDST_PrevBtn");

			next = parseInt(matches[index], 10);
			var miourl = currenturl();
			
			// Delete number sign (if any) followed by numbers at the end of URL
			miourl = miourl.replace(/#\d+$/g, "");

			var variabile = miourl + "#" + next;

			gotoURL(variabile);

			if (index == dim - 1)
				disableBtn("myDST_NextBtn");
		}
	}
}


