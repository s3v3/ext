# README #

Estensione di Iceweasel per la ricerca dei pacchetti Debian, apertura della pagina di un bug e altre operazioni quando  
ci si trova sulla pagina di un bug.

## Changelog ##

#### 1.0.1 *(16/12/15)*
- Upload to AMO

#### 1.0.0 *(27/10/15)*
- Nuova icona di default
- Aggiunto chrome URL alle icone
- Modificato il path delle immagini

#### 0.0.16 *(07/10/15)*
- commenti dei file tradotti in inglese
- corretta interfaccia deprecata nsIFormHistory2
- aggiunto file licenza e copia gpl v.3.
- aggiunto riferimenti licenza ai file .xul e .js
- localizzazioni:
    - install.rdf: aggiunto localizzazione descrizioni.
    - debsearchtoolbar.xul: aggiunto localizzazioni stringhe.
    - aggiunto cartella locale con traduzioni italiano e inglese.
- Reinserito e corretto stringhe "value" nei file .xul e .js
- Modifiche minori generali

#### 0.0.15 *(01/08/15)*
- UUID cambiato
- Il punto di navigazione in una pagina di bug non viene perso al cambio di tab
- install.rdf: aggiornato e aggiunto tag per tipo di add-on.

#### 0.0.14 *(02/4/15)*
- Corretto file .xul
    - riorganizzato la struttura.
    - eliminato alcuni attributi non ritenuti indispensabili.
- Corretto file .css
    - overlay della regola per la dimensione massima della toolbar. In questo modo non viene più tagliata la parte sottostante quando si visualizza a dimensioni ridotte.
    - aggiunto float per il ridimensionamento della toolbar.
    - aggiunto vertical-align ai bottoni per allinearli con il resto delle label.

#### 0.0.13 *(1/4/15)*
- Riorganizzata cartella secondo gli standard di Mozilla
- Riscritto il file debsearchtoolbar.xul seguendo le regole di Mozilla Development.
- Aggiornato il file debsearchtoolbar.css
- Rinominato gli ID in tutti i file (.xul, .js e .css) perché troppo generici e possibili a conflitti con altre estensioni.
- Ripulito codice file .xul

#### 0.0.12 *(11/8/14)*
- Accorciati gli elementi
- Introdotto un abbozzo di CSS
- Si apre la nuova pagina del tracker dei pacchetti anche se ci si trova  
su una pagina di bug (nuovo bottone)

#### 0.0.11 *(7/3/14)*
- I listener creati vengono rimossi una volta utilizzati

#### 0.0.10 *(5/3/14)*
- I bottoni su/giù per i bug vengono abilitati/disabilitati all'occorrenza

#### 0.0.9 *(27/2/14)*
- Apre la pagina in base al numero di bug inserito
- Va su e giù nei messaggi della pagina di bug

#### 0.0.8 *(24/2/14)*
- Aggiunto link ai sorgenti del pacchetto

#### 0.0.7 *(21/9/13)*
- Aggiunto PTS

#### 0.0.6 *(8/11/12)*
- Stringa da cercare cancellata automaticamente dopo ogni ricerca

#### 0.0.5 *(5/5/12)*
- Aggiunta l'history
- Aggiunta qualche icona
